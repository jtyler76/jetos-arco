#!/bin/bash

## --- Request Settings
APIKEY=`cat ${HOME}/.owm-key`
CITY="Ottawa"
COUNTRY="CA"
LANG="en"
UNITS="metric"

## --- Icons
ICON_THUNDER=""
ICON_DRIZZLE_LIGHT_DAY=""
ICON_DRIZZLE_LIGHT_NIGHT=""
ICON_DRIZZLE_MODERATE=""
ICON_DRIZZLE_HEAVY=""
ICON_RAIN_LIGHT_DAY=""
ICON_RAIN_LIGHT_NIGHT=""
ICON_RAIN_MODERATE=""
ICON_RAIN_HEAVY=""
ICON_RAIN_FREEZING=""
ICON_SNOW=""
ICON_MIST=""
ICON_SMOKE=""
ICON_HAZE=""
ICON_DUST=""
ICON_FOG=""
ICON_SAND=""
ICON_VOLCANIC_ASH=""
ICON_SQUALLS=""
ICON_TORNADO=""
ICON_CLEAR_SKIES_DAY=""
ICON_CLEAR_SKIES_NIGHT=""
ICON_PARTLY_CLOUDY_DAY=""
ICON_PARTLY_CLOUDY_NIGHT=""
ICON_OVERCAST=""

## --- Build Request URL
URL_BASE="api.openweathermap.org/data/2.5/weather?"
URL_APPID="appid=${APIKEY}"
URL_UNITS="&units=${UNITS}"
URL_LANG="&lang=${LANG}"
URL_LOCATION="&q=${CITY},${COUNTRY}"
URL="${URL_BASE}${URL_APPID}${URL_UNITS}${URL_LANG}${URL_LOCATION}"

## --- Request Weather Data
RAW_WEATHER_DATA=`curl -s ${URL}`
if [ ${?} -ne 0 ]; then
    echo "unavailable"
fi

## --- Print Raw Data with Debug Option
if [ "${1}" = "-d" ]; then
    echo ${RAW_WEATHER_DATA}
    echo ""
fi

## --- Parse Weather Data
TEMPERATURE=`echo ${RAW_WEATHER_DATA} | jq .main.temp | sed 's/\.[0-9]*//g'`

## --- Send Formatted Weather String to Polybar
echo -n "${TEMPERATURE}°C"
