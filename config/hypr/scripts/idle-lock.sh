#!/bin/bash

swayidle -w                                                \
    timeout 300  '~/.config/hypr/swaylock/lockscreen &'    \
    timeout 600  'hyprctl dispatch dpms off'               \
    resume       'hyprctl dispatch dpms on'                \
    before-sleep '~/.config/hypr/swaylock/lockscreen &'
