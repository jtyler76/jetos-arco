#!/bin/bash

## --- Request Settings
APIKEY=`cat ${HOME}/.owm-key`
CITY="Ottawa"
COUNTRY="CA"
LANG="en"
UNITS="metric"

## --- Icons
ICON_THUNDER=""
ICON_DRIZZLE_LIGHT_DAY=""
ICON_DRIZZLE_LIGHT_NIGHT=""
ICON_DRIZZLE_MODERATE=""
ICON_DRIZZLE_HEAVY=""
ICON_RAIN_LIGHT_DAY=""
ICON_RAIN_LIGHT_NIGHT=""
ICON_RAIN_MODERATE=""
ICON_RAIN_HEAVY=""
ICON_RAIN_FREEZING="󰙿"
ICON_SNOW="󰼶"
ICON_MIST="󰖑"
ICON_SMOKE="󰖑"
ICON_HAZE="󰖑"
ICON_DUST="󰖑"
ICON_FOG="󰖑"
ICON_SAND=""
ICON_VOLCANIC_ASH="󱪃"
ICON_SQUALLS=""
ICON_TORNADO="󰼸"
ICON_CLEAR_SKIES_DAY=""
ICON_CLEAR_SKIES_NIGHT=""
ICON_PARTLY_CLOUDY_DAY=""
ICON_PARTLY_CLOUDY_NIGHT=""
ICON_OVERCAST="󰖐"

## --- Build Request URL
URL_BASE="api.openweathermap.org/data/2.5/weather?"
URL_APPID="appid=${APIKEY}"
URL_UNITS="&units=${UNITS}"
URL_LANG="&lang=${LANG}"
URL_LOCATION="&q=${CITY},${COUNTRY}"
URL="${URL_BASE}${URL_APPID}${URL_UNITS}${URL_LANG}${URL_LOCATION}"

## --- Request Weather Data
RAW_WEATHER_DATA=`curl -s ${URL}`
if [ ${?} -ne 0 ]; then
    echo "unavailable"
fi

## --- Print Raw Data with Debug Option
if [ "${1}" = "-d" ]; then
    echo ${RAW_WEATHER_DATA}
    echo ""
fi

## --- Parse Weather Data
TEMPERATURE=`echo ${RAW_WEATHER_DATA} | jq .main.temp | sed 's/\.[0-9]*//g'`
WEATHER_MAIN=`echo ${RAW_WEATHER_DATA} | jq .weather[0].main`
WEATHER_DESC=`echo ${RAW_WEATHER_DATA} | jq .weather[0].description`
WEATHER_ID=`echo ${RAW_WEATHER_DATA} | jq .weather[0].id`
SUNRISE=`echo ${RAW_WEATHER_DATA} | jq .sys.sunrise`
SUNSET=`echo ${RAW_WEATHER_DATA} | jq .sys.sunset`
TIME=`date +%s`

## --- Create Weather Indicator
## ---   Weather codes: https://openweathermap.org/weather-conditions
# Thunder
if [ ${WEATHER_ID} -ge 200 -a ${WEATHER_ID} -le 232 ]; then
    WEATHER="${ICON_THUNDER}"
fi
# Dizzle, Light
if [ ${WEATHER_ID} -eq 300 -o ${WEATHER_ID} -eq 310 ]; then
    if [ ${TIME} -ge ${SUNRISE} -a ${TIME} -le ${SUNSET} ]; then
        WEATHER="${ICON_DRIZZLE_LIGHT_DAY}"
    else
        WEATHER="${ICON_DRIZZLE_LIGHT_NIGHT}"
    fi
fi
# Drizzle, Moderate
if [ ${WEATHER_ID} -eq 301 -o \
     ${WEATHER_ID} -eq 311 -o \
     ${WEATHER_ID} -eq 313 -o \
     ${WEATHER_ID} -eq 321 ]; then
    WEATHER="${ICON_DRIZZLE_MODERATE}"
fi
# Drizzle, Heavy
if [ ${WEATHER_ID} -eq 302 -o \
     ${WEATHER_ID} -eq 312 -o \
     ${WEATHER_ID} -eq 314 ]; then
    WEATHER="${ICON_DRIZZLE_HEAVY}"
fi
# Rain, Light
if [ ${WEATHER_ID} -eq 500 -o ${WEATHER_ID} -eq 512 ]; then
    if [ ${TIME} -ge ${SUNRISE} -a ${TIME} -le ${SUNSET} ]; then
        WEATHER="${ICON_RAIN_LIGHT_DAY}"
    else
        WEATHER="${ICON_RAIN_LIGHT_NIGHT}"
    fi
fi
# Rain, Moderate
if [ ${WEATHER_ID} -eq 502 -o \
     ${WEATHER_ID} -eq 520 -o \
     ${WEATHER_ID} -eq 521 ]; then
    WEATHER="${ICON_RAIN_MODERATE}"
fi
# Rain, Heavy
if [ ${WEATHER_ID} -eq 503 -o \
     ${WEATHER_ID} -eq 504 -o \
     ${WEATHER_ID} -eq 522 -o \
     ${WEATHER_ID} -eq 531 ]; then
    WEATHER="${ICON_RAIN_HEAVY}"
fi
# Rain, Freezing
if [ ${WEATHER_ID} -eq 511 ]; then
    WEATHER="${ICON_RAIN_FREEZING}"
fi
# Snowing
if [ ${WEATHER_ID} -ge 600 -a ${WEATHER_ID} -le 622 ]; then
    WEATHER="${ICON_SNOW}"
fi
# Mist
if [ ${WEATHER_ID} -eq 701 ]; then
    WEATHER="${ICON_MIST}"
fi
# Smoke
if [ ${WEATHER_ID} -eq 711 ]; then
    WEATHER="${ICON_SMOKE}"
fi
# Haze
if [ ${WEATHER_ID} -eq 721 ]; then
    WEATHER="${ICON_HAZE}"
fi
# Dust
if [ ${WEATHER_ID} -eq 731 -o ${WEATHER_ID} -eq 761 ]; then
    WEATHER="${ICON_DUST}"
fi
# Fog
if [ ${WEATHER_ID} -eq 741 ]; then
    WEATHER="${ICON_FOG}"
fi
# Sand
if [ ${WEATHER_ID} -eq 751 ]; then
    WEATHER="${ICON_SAND}"
fi
# Volcanic Ash
if [ ${WEATHER_ID} -eq 762 ]; then
    WEATHER="${ICON_VOLCANIC_ASH}"
fi
# Squalls
if [ ${WEATHER_ID} -eq 771 ]; then
    WEATHER="${ICON_SQUALLS}"
fi
# Tornado
if [ ${WEATHER_ID} -eq 781 ]; then
    WEATHER="${ICON_TORNADO}"
fi
# Clear Skies
if [ ${WEATHER_ID} -eq 800 ]; then
    if [ ${TIME} -ge ${SUNRISE} -a ${TIME} -le ${SUNSET} ]; then
        WEATHER="${ICON_CLEAR_SKIES_DAY}"
    else
        WEATHER="${ICON_CLEAR_SKIES_NIGHT}"
    fi
fi
# Clouds, Partly Cloudy
if [ ${WEATHER_ID} -eq 801 -o ${WEATHER_ID} -eq 802 ]; then
    if [ ${TIME} -ge ${SUNRISE} -a ${TIME} -le ${SUNSET} ]; then
        WEATHER="${ICON_PARTLY_CLOUDY_DAY}"
    else
        WEATHER="${ICON_PARTLY_CLOUDY_NIGHT}"
    fi
fi
# Clouds, Overcast
if [ ${WEATHER_ID} -eq 803 -o ${WEATHER_ID} -eq 804 ]; then
    WEATHER="${ICON_OVERCAST}"
fi

## --- Send Formatted Weather String to Polybar
echo -n "${WEATHER}"
